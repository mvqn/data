<?php
declare(strict_types=1);

namespace MVQN\Data\Exceptions;

/**
 * Class ModelMissingPropertyException
 *
 * @package MVQN\Data\Exceptions
 * @author Ryan Spaeth <rspaeth@mvqn.net>
 * @final
 */
final class ModelMissingPropertyException extends \Exception
{
}

