<?php
declare(strict_types=1);

namespace MVQN\Data\Exceptions;

/**
 * Class DatabaseConnectionException
 *
 * @package MVQN\Data\Exceptions
 * @author Ryan Spaeth <rspaeth@mvqn.net>
 * @final
 */
final class DatabaseConnectionException extends \Exception
{
}

